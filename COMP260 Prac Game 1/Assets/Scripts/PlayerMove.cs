﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public Vector2 velocity;
	public Vector2 move;
	public string horizontalControl = "Horizontal";
	public string vertialControl= "Vertical" ;
	public float brake = 0.99f;
	public float maxSpeed = 5.0f;
	public float acceleration = 10f;
	private float speed = 0.0f;
	public float turnSpeed = 90.0f; // in degrees/second
	private BeeSpawn beeSpawner;
	public float destroyRadius = 1.0f; 


	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawn> ();
	}

	
	// Update is called once per frame

	void Update() {

		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = -Input.GetAxis(horizontalControl);

		// turn the car
		transform.Rotate(0, 0, turn * turnSpeed * Mathf.Abs(speed) * Time.deltaTime);


		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(vertialControl);
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			speed = speed - (speed *  brake)*Time.deltaTime ;
		}
		// speed clamp
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

	}


}
