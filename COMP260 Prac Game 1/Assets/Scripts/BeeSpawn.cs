﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawn : MonoBehaviour {

	public int nBees = 50;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	public float timer = 0;
	public float BeePeriod = 0.0f;
	public float minBeePeriod = 1.0f;
	public float maxBeePeriod = 2.0f;


	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// Fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}

	void Start () {
		StartCoroutine ("maekbees");
	}
	IEnumerator maekbees() {
		// create bees
		for (int i = 0; i < nBees; i++) {
			// instantiate a bee
			BeeMove bee = Instantiate(beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + i;

			// move the bee to a random position within 
			// the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);
			yield return new WaitForSeconds (Random.Range (minBeePeriod, maxBeePeriod));

		}

	}
		

	
	// Update is called once per frame
	void Update () {
		
	}
}
